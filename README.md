# Guide of use #

**Before the launching:**
First of all you need to install [ROS](http://wiki.ros.org/melodic/Installation/Ubuntu) in your pc (the best one in this case is the Melodic), and than need to initializate the master by typing `roscore` in the terminal. After those steps, go to the devel folder and type `source setup.bash`. 

**How to launch:**
To launch, go to the launch folder and, in the terminal, type `roslaunch gazebo.launch`. This command will open the camera view and the Gazebo app.

**How the code works:**
The code makes Marlin follow Nemo by finding it's color (in the case, red). Than he keep's follow Nemo until he stops. If Marlin doesn't see Nemo, he keeps turning himself, searching. 